#!/bin/bash

# 3x4=12  4X3=12  9+9+3=21 9+3=12 :) here we are 8+3=11 3+8=11 8-3=5

SSH_KEY_PATH=~/.keys/hgc.0.priv &&\
PYTHON3_PIP_VERSION=18.1-5 &&\
PIP3_VIRTUALENV_VERSION=1.9.1  &&\
VV_DEV_BOOTSTRAP_GIT_REPO=git@gitlab.com:vvsw/vv-dev-bootstrap.git  &&\
VV_DEV_BOOTSTRAP_GIT_BRANCH=dev/0.0.1
VV_DEV_BOOTSTRAP_GIT_LOCAL_PATH=/tmp/tmp_vv_dev_bootstrap
VV_DEV_BOOTSTRAP_PLAYBOOK_PATH=${VV_DEV_BOOTSTRAP_GIT_LOCAL_PATH}/vv-dev-bootstrap-playbook.yml


# install vim editor
sudo apt-get install -y vim  &&\
sudo apt-get remove -y nano &&\

# check if ssh-key exists and load key
if [[ ! -f $SSH_KEY_PATH ]]
then
	echo "[fatal]: ssh key does not exist. please provide correct path to private ssh key!"
	echo "[fatal]: please use command ssh-keygen to generate a new key."
	echo "[fatal]: press any key to exit"
	read press_enter_exit
	exit 1
fi

ssh-add -l $SSH_KEY_PATH
sudo apt-get install -y python3-pip=$PYTHON3_PIP_VERSION python3-setuptools  &&\

# install virtualenv via pip3 if not already installed
if [[ "$(pip3 freeze | grep virtualenv)" != "virtualenv=="* ]]
then
	echo "[info] installing virtualenv via pip3"
	pip3 install virtualenv==$PIP3_VIRTUALENV_VERSION
fi

# add virtualenv binary to bash environment path
if [[ ":$PATH:" != *":~/.local/bin':"* ]] 
then
	PATH="~/.local/bin:${PATH}"
fi

virtualenv test_venv_here &&\
source test_venv_here/bin/activate  &&\

# at this point, virtualenv installed and activated
# install ansible and check version
pip3 install ansible==2.9.9  &&\
ansible --version  &&\

# clone via git and run playbook for vv-dev-bootstrap
rm -rf $VV_DEV_BOOTSTRAP_GIT_LOCAL_PATH  &&\
git clone -b $VV_DEV_BOOTSTRAP_GIT_BRANCH --single-branch $VV_DEV_BOOTSTRAP_GIT_REPO $VV_DEV_BOOTSTRAP_GIT_LOCAL_PATH  &&\
 
sudo apt-get install -y python3-apt  &&\
cd $VV_DEV_BOOTSTRAP_GIT_LOCAL_PATH  &&\
ansible-playbook -e ansible_python_interpreter=/usr/bin/python3 $VV_DEV_BOOTSTRAP_PLAYBOOK_PATH





